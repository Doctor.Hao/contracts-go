module encdata

go 1.16

require (
	chainmaker.org/chainmaker/common/v2 v2.3.0
	chainmaker.org/chainmaker/contract-sdk-go/v2 v2.3.2
)
