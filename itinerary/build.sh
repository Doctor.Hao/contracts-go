#!/bin/bash

contractName=$1
if  [[ ! -n $contractName ]] ;then
    echo "contractName is empty. used as: ./build.sh contractName"
    exit 1
fi

CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-s -w" -o $contractName main.go
7z a $contractName $contractName
rm -f $contractName
